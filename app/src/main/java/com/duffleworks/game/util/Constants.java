package com.duffleworks.game.util;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.duffleworks.game.Application;
import com.duffleworks.game.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Orion on 4/16/15.
 */
public class Constants {

    static public int PESOS_1 = 1;
    static public int PESOS_10 = 10;
    static public int PESOS_100 = 100;
    static public int PESOS_5000 = 5000;
    static public int PESOS_10000 = 10000;

    static public float PESOS_SCALE_FACTOR = 0.13f;

    static public float PESOS_ROTATION = 90f;

    static public Map<Integer,Drawable> PESOS_DRAWABLE = new HashMap<>();
    static public Map<Integer,Float> PESOS_START_X = new HashMap<>();
    static public Map<Integer,Float> PESOS_START_Y = new HashMap<>();

    static public void setPesosDrawables(){
        PESOS_DRAWABLE.put(PESOS_1, Application.getContext().getResources().getDrawable(R.drawable.peso_1));
        PESOS_DRAWABLE.put(PESOS_10, Application.getContext().getResources().getDrawable(R.drawable.peso_10));
        PESOS_DRAWABLE.put(PESOS_100, Application.getContext().getResources().getDrawable(R.drawable.peso_100));
        PESOS_DRAWABLE.put(PESOS_5000, Application.getContext().getResources().getDrawable(R.drawable.peso_5000));
        PESOS_DRAWABLE.put(PESOS_10000, Application.getContext().getResources().getDrawable(R.drawable.peso_10000));
    }

    static public void setPesosStartX(){
        PESOS_START_X.put(PESOS_10000,-145f);
        PESOS_START_X.put(PESOS_5000,-75f);
        PESOS_START_X.put(PESOS_100,10f);
        PESOS_START_X.put(PESOS_10,107f);
        PESOS_START_X.put(PESOS_1,160f);
    }

    static public void setPesosStartY(){
        PESOS_START_Y.put(PESOS_1,-40f);
        PESOS_START_Y.put(PESOS_10,-18f);
        PESOS_START_Y.put(PESOS_100,-40f);
        PESOS_START_Y.put(PESOS_5000,23f);
        PESOS_START_Y.put(PESOS_10000,45f);
    }

    static public String RESULT_CORRECT = "correct";
    static public String RESULT_WRONG = "wrong";
}

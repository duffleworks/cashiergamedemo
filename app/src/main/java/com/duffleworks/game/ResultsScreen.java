package com.duffleworks.game;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class ResultsScreen extends ActionBarActivity {

    TextView numRigth, numWrong;
    ImageView homeButton;

    int rights = 0;
    int wrongs = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_screen);

        setViewElements();

        rights = getIntent().getIntExtra("numRight", 0);
        wrongs = getIntent().getIntExtra("numWrong", 0);

        setViewElements();
    }

    private void setViewElements() {
        numRigth = (TextView) findViewById(R.id.numRight);
        numWrong = (TextView) findViewById(R.id.numWrong);

        numRigth.setText(Integer.toString(rights));
        numWrong.setText(Integer.toString(wrongs));

        homeButton = (ImageView) findViewById(R.id.home);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_results_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

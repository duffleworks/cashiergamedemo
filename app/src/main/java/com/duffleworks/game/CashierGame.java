package com.duffleworks.game;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.duffleworks.game.util.Constants;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class CashierGame extends Activity {

    ImageView cashier, tryAgainMessage, rightAnswerMessage;
    ImageView backArrow, forwardArrow, forwardArrowGrey, homeButton;
    TextView cashierDisplay, hint;

    RelativeLayout scene, helpContainer;

    int totalAmount = 0;

    View selectedItem = null;
    float originX = 0f;
    float originY = 0f;
    int selectedAmount = 0;

    float touchOriginX = -10000;
    float touchOriginY = -10000;
    float deltaX = 0;
    float deltaY = 0;

    int goalValue = 30000;

    int numRight = 0;
    int numWrong = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashier_game);

        scene = (RelativeLayout) findViewById(R.id.gameScene);
        helpContainer = (RelativeLayout) findViewById(R.id.helpContainer);

        cashier = (ImageView) findViewById(R.id.cashier);
        cashierDisplay = (TextView) findViewById(R.id.cashierDisplay);
        hint = (TextView) findViewById(R.id.hint);

        scene.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //For checking if money is on the cashier
                boolean moneyOnCashier = false;
                if (selectedItem != null) {
                    moneyOnCashier = selectedItem.getX() > cashier.getX() - cashier.getWidth() && selectedItem.getY() > cashier.getY() - cashier.getHeight()
                            && selectedItem.getX() < cashier.getX() && selectedItem.getY() < cashier.getY();
                }

                switch (event.getActionMasked()) {

                    case MotionEvent.ACTION_DOWN:

                        touchOriginX = event.getRawX();
                        touchOriginY = event.getRawY();

                        setOnGame();

                        break;
                    case MotionEvent.ACTION_MOVE:

                        float x = event.getRawX();
                        float y = event.getRawY();

                        deltaX = x - touchOriginX;
                        deltaY = y - touchOriginY;

                        if (selectedItem == null) break;

                        selectedItem.setX(originX + deltaX);
                        selectedItem.setY(originY + deltaY);

                        if (moneyOnCashier) {
                            cashier.setImageDrawable(Application.getContext().getResources().getDrawable(R.drawable.cashier_open));
                        }
                        else {
                            cashier.setImageDrawable(Application.getContext().getResources().getDrawable(R.drawable.cashier_closed));
                        }

                        break;
                    case MotionEvent.ACTION_UP:
                        if (selectedItem == null) break;

                        //System.out.println(selectedItem.getX() + " " + cashier.getX());

                        if (moneyOnCashier) {
                            System.out.println(selectedItem.getX() + " " + cashier.getX());
                            totalAmount = totalAmount + selectedAmount;

                            DecimalFormat formatter = new DecimalFormat("###,###");
                            String displayText = formatter.format(totalAmount);

                            cashierDisplay.setText(displayText);
                            scene.removeView(selectedItem);
                            addMoney(selectedAmount);

                            checkCashierValue();

                            //cashier.setImageDrawable(Application.getContext().getResources().getDrawable(R.drawable.cashier_closed));

                        }
                        selectedItem.setX(originX);
                        selectedItem.setY(originY);

                        scene.removeView(helpContainer);
                        scene.addView(helpContainer);

                        selectedItem = null;
                        break;

                    default:
                        break;
                }
                return true;
            }
        });

        addMoney(Constants.PESOS_10000);
        addMoney(Constants.PESOS_5000);
        addMoney(Constants.PESOS_100);
        addMoney(Constants.PESOS_10);
        addMoney(Constants.PESOS_1);

        setFlowButtons();
        setCashierMessages();

        resetGame();

    }

    private void setOnGame() {
        helpContainer.setVisibility(View.GONE);

        cashier.setImageDrawable(Application.getContext().getResources().getDrawable(R.drawable.cashier_closed));

        cashierDisplay.setVisibility(View.VISIBLE);
        tryAgainMessage.setVisibility(View.GONE);
        rightAnswerMessage.setVisibility(View.GONE);
    }

    private void setFlowButtons() {
        backArrow = (ImageView) findViewById(R.id.arrowBack);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        forwardArrow = (ImageView) findViewById(R.id.arrowForward);
        forwardArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent(CashierGame.this, ResultsScreen.class);

                resultIntent.putExtra("numRight",numRight);
                resultIntent.putExtra("numWrong",numWrong);
                startActivity(resultIntent);
                finish();
            }
        });

        forwardArrowGrey = (ImageView) findViewById(R.id.arrowForwardGrey);
    }

    private void checkCashierValue() {

        if (totalAmount > goalValue) {
            cashierDisplay.setVisibility(View.GONE);
            tryAgainMessage.setVisibility(View.VISIBLE);

            cashier.setImageDrawable(Application.getContext().getResources().getDrawable(R.drawable.cashier_closed));

            //+1 wrong
            numWrong++;
            playAudio(Constants.RESULT_WRONG);
            resetGame();
        }
        else if (totalAmount == goalValue) {
            cashierDisplay.setVisibility(View.GONE);
            rightAnswerMessage.setVisibility(View.VISIBLE);

            //+1 right
            playAudio(Constants.RESULT_CORRECT);
            numRight++;
            cashier.setImageDrawable(Application.getContext().getResources().getDrawable(R.drawable.cashier_happy_close));

            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            cashier.setImageDrawable(Application.getContext().getResources().getDrawable(R.drawable.cashier_closed));
                        }
                    });

                }
            };
            new Timer().schedule(task, 1*1000);

            if (numRight >= 3) {
                forwardArrow.setVisibility(View.VISIBLE);
                forwardArrowGrey.setVisibility(View.GONE);

                task.cancel();
                hint.setText("¡Juego completado!");

                scene.setOnTouchListener(null);

                return;
            }

            resetGame();
        }
        else {
            cashier.setImageDrawable(Application.getContext().getResources().getDrawable(R.drawable.cashier_closed));
        }


    }

    private void playAudio(String result) {
        MediaPlayer audio = null;

        if (Constants.RESULT_CORRECT.equals(result)) audio = MediaPlayer.create(this, R.raw.correct);
        else if (Constants.RESULT_WRONG.equals(result)) audio = MediaPlayer.create(this, R.raw.wrong);

        audio.start();
    }

    private void resetGame(){
        if (numWrong >= 3 && numWrong % 3 == 0) {
            helpContainer.setVisibility(View.VISIBLE);
        }

        totalAmount = 0;
        cashierDisplay.setText(Integer.toString(totalAmount));
        goalValue = getRandomNumber();

        DecimalFormat formatter = new DecimalFormat("###,###");
        String hintValue = formatter.format(goalValue);

        hint.setText("Ingresa "+hintValue+" pesos a la caja");
    }

    private int getRandomNumber(){
        //Generate random multiple of 5000
        int multiple = 5000;
        int max = 10;
        int min = 2;

        int randomOf5000 = multiple * new Random().nextInt(max - min +1) + min;

        //Generate random between 0 and 999
        max = 999;
        min = 0;

        int random = new Random().nextInt(max - min +1) + min;

        return randomOf5000 + random;

    }

    private void setCashierMessages() {
        tryAgainMessage = (ImageView) findViewById(R.id.tryAgainMessage);
        tryAgainMessage.setVisibility(View.GONE);

        rightAnswerMessage = (ImageView) findViewById(R.id.righAnswerMessage);
        rightAnswerMessage.setVisibility(View.GONE);
    }

    private void addMoney(final int amount) {

        final ImageView image = new ImageView(this);
        image.setImageDrawable(Constants.PESOS_DRAWABLE.get(amount));

        float scaleFactor = Constants.PESOS_SCALE_FACTOR;
        image.setScaleType(ImageView.ScaleType.FIT_XY);
        image.setScaleX(scaleFactor);
        image.setScaleY(scaleFactor);
        image.setRotation(Constants.PESOS_ROTATION);

        final float x = Constants.PESOS_START_X.get(amount);
        final float y = Constants.PESOS_START_Y.get(amount);

        image.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:

                        selectedItem = v;
                        originX = x;
                        originY = y;
                        selectedAmount = amount;

                        break;
                    default:
                        break;
                }
                // return false to ignore further MotionEvents
                // and give the parent a chance to handle MotionEvent
                return false;
            }
        });

        image.setX(x);
        image.setY(y);

        scene.addView(image);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cashier_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}



package com.duffleworks.game;

import android.content.Context;

/**
 * Created by Orion on 4/16/15.
 */
public class Application extends android.app.Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext() {
        return mContext;
    }
}
